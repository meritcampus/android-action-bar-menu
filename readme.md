# Android Action Bar Menu

In this example we will be discussing about the **action bar menu** in Android.

### Components you will learn
  - Activity
  - SQL Database
  - Action Bar
 
**Action Bar :** The action bar is an important design element, usually at the top of each screen in an app, that provides a consistent familiar look between Android apps. It is used to provide better user interaction and experience by supporting easy navigation through tabs and drop-down lists.


- To add buttons in Action Bar goto Android project -> res -> menu -> main.xml Inside main.xml we can add as many action bars buttons we want.

- We should override **onCreateOptionsMenu(Menu menu)** method it will show all the menu items. Using setTitle() method we can set the title of the Action Bar

- Inside manifiest file We can change the color of the action bar using  android:theme attribute.
 
- We can change the color of the action bar programatically using setBackgroundDrawable() method.

- We can perform on click action for the action bar buttons using  **onOptionsItemSelected(MenuItem item)** method. 

- We can add the icon using android:icon attribute inside main.xml file and using **setIcon()** method programatically.
- Using **showAsAction** attribute we can set the action bar items on Action Bar or in the room of Action Bar (ifroom, never, withText, always etc...).